import pandas as pd
import re
from sentiment_analysis_spanish import sentiment_analysis

#lista para corregir algunas palabras coloquiales / jerga en español
SLANG = [('d','de'), ('[qk]','que'), ('xo','pero'), ('xa', 'para'), ('[xp]q','porque'),('es[qk]', 'es que'),
         ('fvr','favor'),('(xfa|xf|pf|plis|pls|porfa)', 'por favor'), ('dnd','donde'), ('tb', 'también'),
         ('(tq|tk)', 'te quiero'), ('(tqm|tkm)', 'te quiero mucho'), ('x','por'), ('\+','mas')]

NORMALIZE = 'normalize'
REMOVE = 'remove'
# Palabras con las que se van a reemplazar las menciones, hashtags, URLs y las risas
MENTION = ''
HASHTAG = ''
URL = ''
LAUGH = ''
# variable para definir si quiero normalizar: normalize o eliminar (remove): elimina los hashtags,
# menciones y urls en los tweets
_twitter_features="normalize"


# Normalizar/Eliminar risas
def normalize_laughs(message):
  message = re.sub(r'\b(?=\w*[j])[aeiouj]{4,}\b', LAUGH, message, flags=re.IGNORECASE)
  message = re.sub(r'\b(?=\w*[k])[aeiouk]{4,}\b', LAUGH, message, flags=re.IGNORECASE)
  message = re.sub(r'\b(?=\w*[h])[aeiouk]{4,}\b', LAUGH, message, flags=re.IGNORECASE)
  message = re.sub(r'\b(juas+|lol)\b', LAUGH, message, flags=re.IGNORECASE)
  return message

def process_twitter_features(message, twitter_features):

  message = re.sub(r'[\.\,]http','. http', message, flags=re.IGNORECASE)
  message = re.sub(r'[\.\,]#', '. #', message)
  message = re.sub(r'[\.\,]@', '. @', message)

  if twitter_features == REMOVE:
    # eliminar menciones, hashtags y URL
    message = re.sub(r'((?<=\s)|(?<=\A))(@|#)\S+', '', message)
    message = re.sub(r'\b(https?:\S+)\b', '', message, flags=re.IGNORECASE)
  elif twitter_features == NORMALIZE:
    # cuando sea necesario se normalizaran las menciones, hashtags y URL
    message = re.sub(r'((?<=\s)|(?<=\A))@\S+', MENTION, message)
    message = re.sub(r'((?<=\s)|(?<=\A))#\S+', HASHTAG, message)
    message = re.sub(r'\b(https?:\S+)\b', URL, message, flags=re.IGNORECASE)

  return message

# La función preprocesamiento elimina o transforma cosas para que sea más facil leer el texto
def preprocess(message):
  # convertir a minúsculas
  message = message.lower()
        
  # eliminar números, retorno de linea y el retweet
  message = re.sub(r'(\d+|\n|\brt\b)', '', message)
        
  # eliminar caracteres repetidos 
  message = re.sub(r'(.)\1{2,}', r'\1\1', message)
       
  # normalizar las risas
  message = normalize_laughs(message)
        
  # traducir la jerga y terminos coloquiales sobre todo en el español
  for s,t in SLANG:
    message = re.sub(r'\b{0}\b'.format(s), t, message)

  # normalizar/eliminar hashtags, menciones y URL
  message = process_twitter_features(message, _twitter_features)

  return message


# Ahora abrimos el csv que contiene los tweets como dataframe
df = pd.read_csv('datatwitter.csv', encoding='utf-8')
# asignamos nombres a las columnas del csv para facilitar la busqueda de información
df.columns = ['tweetid', 'tweet']
# a la columna tweet se le aplica la funcion de preprocesamiento
df['tweet'] = df['tweet'].apply(preprocess)

sentiment = sentiment_analysis.SentimentAnalysisSpanish()
# se aplica el análisis de sentimientos a cada valor de la columna tweet
# y se guarda en una nueva columna llamada sentimiento
df['sentimiento'] = df['tweet'].apply(lambda i: sentiment.sentiment(i))
# Finalmente guardamos el dataframe en un nuevo csv
df.to_csv('data_con_sent.csv')


# Ejemplos de prueba con el SentimentAnalysisSpanish


# Más o menos se saca así, falla en algunas cosas:
# sentiment = sentiment_analysis.SentimentAnalysisSpanish()
# print(sentiment.sentiment("Me gusta la tombola es genial")) # Resultado: 0.9304396176531412
# print(sentiment.sentiment("esto me parece terrible"))       # Resultado: 0.00016103507215734614
# La cosa es encontrar o mejor data o usar machine learning